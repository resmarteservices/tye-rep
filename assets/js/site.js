
$("html").click(function() {
	 $(".navbar-collapse").collapse('hide');
});
// Language modal
$(document).on('click', '.lang-trigger', function(e){
	e.preventDefault();
	offsetY = window.pageYOffset;
	$('.modal-overlay.lang, body').addClass('on');
	$('body.on').css({'top':-offsetY+'px'});
});
// Mobile menu trigger
$(document).on('click', '.mobile-trigger', function(e){
	e.preventDefault();
	$('body').addClass('slideout');
});
// Mobile menu close
$(document).on('click', '.mask, #mmenu a.lang-trigger, #mmenu a.search-trigger, #mmenu a.signup-trigger, #mmenu a.login-trigger', function(e){
	e.preventDefault();
	$('body').removeClass('slideout');
});



